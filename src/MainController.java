import java.util.ArrayList;

public class MainController {
    public static void main(String[] args) throws Exception {
        Raumschiff klingonen=new Raumschiff("IKS Hegh'ta",100,100,100,100,1,2,null);
        Raumschiff romulaner=new Raumschiff("IRW Khazara",100,100,100,100,2,2,null);
        Raumschiff vulkanier=new Raumschiff("Ni'Var",80,80,100,50,0,5,null);
        klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
        klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert",200));
        romulaner.addLadung(new Ladung("Borg-Schrott",5));
        romulaner.addLadung(new Ladung("Rote Materie",2));
        romulaner.addLadung(new Ladung("Plasma Waffe",50));
        vulkanier.addLadung(new Ladung("Forschungssonde",35));
        vulkanier.addLadung(new Ladung("Photonentorpedo",3));
        ArrayList<Raumschiff> raumschiffe=new ArrayList<>();
        raumschiffe.add(klingonen);
        raumschiffe.add(romulaner);
        raumschiffe.add(vulkanier);
        //Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.
        klingonen.photonentorpedoAbschiessen(romulaner);
        //Die Romulaner schießen mit der Phaserkanone zurück.
        romulaner.phaserkanoneAbschiessen(klingonen);
        //Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch\n\n");
        //Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
        klingonen.printZustand();
        klingonen.printLadungsverzeichnis();
        //Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
        vulkanier.reperaturAuftragSenden(true,true,true,10);
        //Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf.
        vulkanier.torpedosLaden(10);
        vulkanier.ladungsverzeichnisAufraeumen();
        //Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
        for(int i=0;i<2;i++) {
            klingonen.photonentorpedoAbschiessen(romulaner);
        }
        //Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
        for(Raumschiff r:raumschiffe){
            r.printZustand();
            r.printLadungsverzeichnis();
        }
        //Geben Sie den broadcastKommunikator aus.
        vulkanier.printBroadcastCommunicator();
    }
}