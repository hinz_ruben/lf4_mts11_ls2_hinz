import java.util.ArrayList;
import java.util.Random;
/**
 * @author Ruben Hinz
 * @version 1.1
 * @since 30.03.2022
 * */
public class Raumschiff {
    private String schiffname;
    private int energieversorgung;
    private int schutzschild;
    private int lebenserhaltungssysteme;
    private int huelle;
    private int photonentorpedo;
    private int reperaturAndroiden;
    private ArrayList<Ladung> ladungsverzeichnis;
    private static ArrayList<String> broadcastCommunicator=new ArrayList<>();

    Raumschiff(){

    }
    /**
     * Vollparametrisierter Konstruktor
     * @param schiffname Name des Schiffes
     * @param energieversorgung Energieversorgung in Prozent
     * @param schutzschild Schutzschild in Prozen
     * @param lebenserhaltungssysteme Lebenserhaltungssysteme in Prozent
     * @param huelle Huelle in Prozent
     * @param photonentorpedo Anzahl an Photonentorpedos
     * @param reperaturAndroiden Anzahl an Reperaturandroiden
     * @param ladungsverzeichnis ArrayList mit Ladung, wenn null wird es im Konstruktor leer initialisiert
     * */
    public Raumschiff(String schiffname, int energieversorgung, int schutzschild, int lebenserhaltungssysteme, int huelle,
               int photonentorpedo, int reperaturAndroiden, ArrayList<Ladung> ladungsverzeichnis) {
        setSchiffname(schiffname);
        setEnergieversorgung(energieversorgung);
        setSchutzschild(schutzschild);
        setLebenserhaltungssysteme(lebenserhaltungssysteme);
        setHuelle(huelle);
        setPhotonentorpedo(photonentorpedo);
        setReperaturAndroiden(reperaturAndroiden);
        if(ladungsverzeichnis!=null) {
            setLadungsverzeichnis(ladungsverzeichnis);
        }else{
            setLadungsverzeichnis(new ArrayList<>());
        }
    }
    /**fügt Ladung zum Raumschiff hinzu
     * @param neueLadung Ladung, die dem Ladungsverzeichnis hinzugefügt wird.
     */
    public void addLadung(Ladung neueLadung){
        //überprüfen ob die Ladung schon vorhanden ist
        for(Ladung aktLadung:getLadungsverzeichnis()){
            if(neueLadung.getName().equals(aktLadung.getName())){
                aktLadung.setAnzahl(aktLadung.getAnzahl()+neueLadung.getAnzahl());
            }
        }

        this.ladungsverzeichnis.add(neueLadung);
    }

    public void printZustand(){
        System.out.printf("Name: %s \n",getSchiffname());
        System.out.printf("Energieversorgung: %d%s \n",getEnergieversorgung(),"%");
        System.out.printf("Schutzschild: %d%s \n",getSchutzschild(),"%");
        System.out.printf("Lebenserhaltungsysteme: %d%s \n",getLebenserhaltungssysteme(),"%");
        System.out.printf("Huelle: %d%s \n",getHuelle(),"%");
        System.out.printf("Photonentorpedo: %d \n",getPhotonentorpedo());
        System.out.printf("Anzahl Reperatur Androiden: %d \n\n",getReperaturAndroiden());
    }

    public void printLadungsverzeichnis(){
        System.out.printf("Ladung der %s\n",getSchiffname());
        for (Ladung aktLadung:getLadungsverzeichnis()) {
            System.out.printf("%s",aktLadung.toString());
        }
        System.out.println();
    }

    /**Schießt Photonentorpedo auf Ziel
     * Vorraussetzung: photonentorpedos sind geladen
     * @param ziel von diesem Raumschiff wird die Methode getroffenWerden aufgerufen
     * */
    public void photonentorpedoAbschiessen(Raumschiff ziel){
        if(getPhotonentorpedo()<1){
            nachrichtAnAlle("-=*Click*=-\n\n");
        }else{
            nachrichtAnAlle("Photonentorpedo auf "+ziel.getSchiffname()+" abgeschossen\n\n");
            setPhotonentorpedo(getPhotonentorpedo()-1);
            ziel.getroffenWerden();
        }
    }
    /**Voraussetzung: mindestens 50 Energieversorgung
     * @param ziel von diesem Raumschiff wird die Methode getroffenWerden aufgerufen
     * */
    public void phaserkanoneAbschiessen(Raumschiff ziel){
        if(getEnergieversorgung()<50){
            nachrichtAnAlle("-=*Click*=-\n\n");
        }else{
            nachrichtAnAlle("Phaserkanone auf "+ziel.getSchiffname()+" abgeschossen\n\n");
            setEnergieversorgung(getEnergieversorgung()-50);
            ziel.getroffenWerden();
        }
    }

    private void getroffenWerden(){
        nachrichtAnAlle(getSchiffname()+" wurde getroffen\n\n");
        setSchutzschild(getSchutzschild()-50);
        if(getSchutzschild()==0){
            setHuelle(getHuelle()-50);
            setEnergieversorgung(getEnergieversorgung()-50);
            }
        if(getHuelle()<=0){
            nachrichtAnAlle("Lebenserhaltungssysteme von "+getSchiffname()+" wurden zerstoert\n\n");
        }
    }
    /**Voraussetzung: Photonentorpedos im Ladungsverzeichnis
     * @param anzahl fügt anzahl oder im Ladungsverzeichnis vorhandene anzahl an photonentorpedos dem Raumschiff hinzu
     * */
    public void torpedosLaden(int anzahl){
        boolean ladenErfolgreich=false;
        for (Ladung ladung:getLadungsverzeichnis()) {
            if (ladung.getName()=="Photonentorpedo"){
                if(anzahl<=ladung.getAnzahl()){
                    ladung.setAnzahl(ladung.getAnzahl()-anzahl);
                    setPhotonentorpedo(getPhotonentorpedo()+anzahl);
                    ladenErfolgreich=true;
                    nachrichtAnAlle(anzahl+" Photonentorpedos eingesetzt\n\n");
                }else {
                    setPhotonentorpedo(getPhotonentorpedo()+ladung.getAnzahl());
                    ladenErfolgreich=true;
                    nachrichtAnAlle(ladung.getAnzahl()+" Photonentorpedos eingesetzt\n\n");
                    ladung.setAnzahl(0);
                }
            }
        }
        if (!ladenErfolgreich) {
            System.out.printf("Keine Photonentorpedos geladen\n\n");
        }
    }
    /**Entfernt Ladung mit Anzahl 0
     * */
    public void ladungsverzeichnisAufraeumen(){
        int laenge=0;
        while(laenge<getLadungsverzeichnis().size()) {
            if (getLadungsverzeichnis().get(laenge).getAnzahl()==0){
                ladungsverzeichnis.remove(ladungsverzeichnis.get(laenge));
                laenge--;
            }
            laenge++;
        }
    }
    /**Voraussetzung: Raperaturandroiden vorhanden
     * @param schilde wenn true werden schilde repariert
     * @param huelle wenn true wird die Huelle repariert
     * @param energieversorgung wenn true wird die energieversorgung repariert
     * @param anzahl Anzahl der eingesetzten ReperaturAndroiden
     * */
    public void reperaturAuftragSenden(boolean schilde,boolean huelle,boolean energieversorgung,int anzahl){
        Random rand = new Random();
        int x =rand.nextInt(99)+1;
        int anzahlReperaturen=0;
        if (schilde) {
            anzahlReperaturen += 1;
        }
        if (huelle){
            anzahlReperaturen+=1;
        }
        if (energieversorgung) {
            anzahlReperaturen += 1;
        }
        if(anzahl>getReperaturAndroiden()) {
            anzahl=getReperaturAndroiden();
        }
        int reperaturProzent=x*anzahl/anzahlReperaturen;
        if (schilde) {
            setSchutzschild(getSchutzschild()+reperaturProzent);
        }
        if (huelle){
            setHuelle(getHuelle()+reperaturProzent);
        }
        if (energieversorgung) {
            setEnergieversorgung(getEnergieversorgung()+reperaturProzent);
        }
    }
    /** Eine Nachricht wird an den Broadcastcommunicator übergeben
     * @param nachricht String, der dem Broadcastcommunicator hinzugefügt wird
     * */
    public void nachrichtAnAlle(String nachricht){
        System.out.printf(getSchiffname()+": "+nachricht);
        broadcastCommunicator.add(getSchiffname()+": "+nachricht);
    }

    public String getSchiffname() {
        return schiffname;
    }

    public void setSchiffname(String schiffname) {
        this.schiffname = schiffname;
    }

    public int getEnergieversorgung() {
        return energieversorgung;
    }
    /**setzt den Wert der Energieversorgung fest
     * @param energieversorgung ist der wert <0 wird er auf 0 gesetzt, ist er >100 wird er auf 100 gesetzt
     */
    public void setEnergieversorgung(int energieversorgung) {
        if(energieversorgung<0){
            this.energieversorgung =0;
        }else if(energieversorgung>100){
            this.energieversorgung = 100;
        }else {
            this.energieversorgung = energieversorgung;
        }
    }

    public int getSchutzschild() {
        return schutzschild;
    }
    /**setzt den Wert des Schutzschilds fest
     * @param schutzschild ist der wert <0 wird er auf 0 gesetzt, ist er >100 wird er auf 100 gesetzt
     */
    public void setSchutzschild(int schutzschild) {
        if(schutzschild<0){
            this.schutzschild =0;
        }else if(schutzschild>100){
            this.schutzschild = 100;
        }else {
            this.schutzschild = schutzschild;
        }
    }

    public int getLebenserhaltungssysteme() {
        return lebenserhaltungssysteme;
    }
    /**setzt den Wert der Lebenserhaltungssysteme fest
     * @param lebenserhaltungssysteme ist der wert <0 wird er auf 0 gesetzt, ist er >100 wird er auf 100 gesetzt
     */
    public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
        if(lebenserhaltungssysteme<0){
            this.lebenserhaltungssysteme =0;
        }else if(lebenserhaltungssysteme>100){
            this.lebenserhaltungssysteme = 100;
        }else {
            this.lebenserhaltungssysteme = lebenserhaltungssysteme;
        }
    }

    public int getHuelle() {
        return huelle;
    }
    /**setzt den Wert der Huelle fest
     * @param huelle ist der wert <0 wird er auf 0 gesetzt, ist er >100 wird er auf 100 gesetzt
     */
    public void setHuelle(int huelle) {
        if(huelle<0){
            this.huelle =0;
        }else if(huelle>100){
            this.huelle = 100;
        }else {
            this.huelle = huelle;
        }
    }

    public int getPhotonentorpedo() {
        return photonentorpedo;
    }

    public void setPhotonentorpedo(int photonentorpedo) {
        this.photonentorpedo = photonentorpedo;
    }

    public int getReperaturAndroiden() {
        return reperaturAndroiden;
    }

    public void setReperaturAndroiden(int reperaturAndroiden) {
        this.reperaturAndroiden = reperaturAndroiden;
    }

    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    public ArrayList<String> getBroadcastCommunicator() {
        return broadcastCommunicator;
    }
    /**
     * gibt den BCC auf der Konsole aus
     * */
    public void printBroadcastCommunicator(){
        for(String s:getBroadcastCommunicator()){
            System.out.printf(s);
        }
    }

}
