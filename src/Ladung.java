/**
 * @author Ruben Hinz
 * @version 1.1
 * @since 30.03.2022
 * */
public class Ladung {
    private String name;
    private int anzahl;
    Ladung(){

    }
    /**vollparametrisierter Konstruktor
     * @param name Bezeichnung der Ladung
     * @param anzahl Anzahl der Ladung
     */
    public Ladung(String name, int anzahl){
        this.name=name;
        this.anzahl=anzahl;
    }
    /**gibt die Ladung als String zurück
     * @return String mit Inhalt der Ladung
     */
    @Override
    public String toString() {
        String erg="";
        erg=this.anzahl+"* "+this.name+"\n";
        return erg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
}
